% \begin{itemize}
% \item component-based commissioning model
% \item control components to handle the commissioning of any existing
%   component in any language
% \item goals: programmable, composable, reusable, efficient
% \item composed of:
%   \begin{itemize}
%   \item meta-model
%   \item graphical language
%   \item concrete language
%   \item formal specification (next section)
%   \item formal operational semantics (next section)
%   \end{itemize}
% \item meta-models of assembly and components (add groups)
% \item example Apache/MariaDB with graphical and concrete syntax
% \item simplified explanation of the operational semantics (middleware)
% \item discussion on the goals
% \end{itemize}

%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Principles}

\mad is a procedural \emph{configuration tool} relying on a
component-based coordination model for commissioning
procedures. Inspired by \aeolus, it enhances automation, separation of
concerns and level of parallelism for distributed software
commissioning. Usually, component models are used to write distributed
software in a modular fashion with guarantees on composition,
thus improving separation of concerns between developers, promoting
reuse of existing pieces of code, and enhancing flexibility and
maintainability of code. \mad brings these properties to commissioning
design instead of functional code design.

A \mad component is called a \emph{control} component, as it is only
intended to model the commissioning procedure of an already existing
piece of software code (functional component, module or service). A
\mad control component is a type containing an internal net inspired by Petri
nets, where places represent milestones of the commissioning, and
transitions between places represent actions to be performed (\eg
\texttt{apt-get install}, \texttt{docker pull}, etc.). If multiple
transitions leave a place, their parallel execution is automatically
handled and synchronized by \mad. A component may have dependencies
with other components. These dependencies are declared through ports,
a well-known concept in component-based software engineering. Two
types of ports, working in pairs, are available in \mad:
\emph{provide} and \emph{use} ports. However, both are
\emph{coordination} ports, \ie they model dependencies or
synchronization but do not implement them. By using coordination
ports, each component type can be defined independently and component
instances can be connected later by another developer, thus improving
separation of concerns and reusability of commissioning procedures. A
provide coordination port is bound to sets of places that represents
the milestones that have to be reached for the service or data to be
offered. Finally, use coordination ports are bound to one or multiple
transitions where data and service is actually required.

The overall commissioning procedure of a distributed software system
is built by composition in an \emph{assembly}, where component types
are instantiated and connected. All control components execute
simultaneously, thus introducing inter-component parallelism, in
addition to the intra-component parallelism offered by parallel
transitions. Two components connected by their respective compatible
ports will automatically be coordinated so that a component cannot use
a service or data if the associated provide port is not enabled.

\mad offers the expressiveness required to design composable and parallel
commissioning procedures for complex distributed software systems.

\begin{figure}[tbp]
  \begin{center}
    \includegraphics[width=0.7\linewidth]{./images/apachebdd.pdf}
  \end{center}
  \caption{Example of a \mad commissioning assembly with two components
    Apache and MariaDB. Places are represented by circles, with attached
    docks represented by small squares. Transitions are represented
    by arrows between the docks, and service ports by small black circles and
    semi-circles, data ports by outgoing or incoming arrows from
    components. \revised{Bindings of coordination ports are represented by dashed lines between a port and the places and transitions it is bound to. Tokens (in red) are placed in the initial
    places of each components in this example.}}
  \label{fig:example}
\end{figure}

\paragraph{Example}{
	Figure~\ref{fig:example} depicts a \mad commissioning assembly of an Apache 
	web server and a MariaDB database, using the graphical notation of \mad. 
	This example is based on a real container-based deployment described by 
	RedHat\footnote{\url{https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux_atomic_host/7/html/getting_started_with_containers/install_and_deploy_an_apache_web_server_container}}\footnote{\url{https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux_atomic_host/7/html/getting_started_with_containers/install_and_deploy_a_mariadb_container}}.
	 Two \mad control components are declared in this example: Apache and MariaDB. 
	Apache contains four places (white circles), or milestones, while MariaDB 
	contains five places. Some parallel transitions are declared for each of the 
	component and can be observed in the figure (parallel arrows). Both components 
	have two coordination ports. MariaDB provides both data and a service, while 
	Apache uses a service and data. These instances are connected by their ports. 
	Indeed, the Apache configuration depends on the IP address of the MariaDB 
	component, and the testing phase for Apache, called \texttt{check}, uses the 
	MariaDB service. \revised{Each coordination port is bound to internal elements. This is depicted by a dashed line between ports and places or transitions. For instance, the provide port \texttt{@IP mdb} is bound to the second place of the MariaDB component, and the first use port of the component Apache is bound to the transition \texttt{conf}}.
} %\CP{Ne faut il pas dire quelque part que les ports ne passent que les info de synchro et pas les données/services eux-même ? Par exemple est-ce \mad qui transfére les données comme l'@IP mdb?}

\mad involves two kinds of actors. First is the \emph{developer} of a
control component, who may be the author of the associated existing
piece of code, another developer, or even a system operator or
administrator. Second is the \emph{devops} engineer who designs an
assembly, \ie writes the overall commissioning procedure of a
distributed software system to deploy on an infrastructure. \mad
offers clear separation of concerns between the commissioning of a
single component on the one hand, and the composition of an assembly
on the other. The latter does not require detailed information about
the commissioning of each component. This is a benefit compared to
existing solution. For instance, even if Ansible offers properties
close to composition (\eg roles, playbooks, tasks, etc.), the devops
still has to determine the correct order of composition. By contrast,
the correct coordination, hence the correct order of execution, is
automatically guaranteed by the \mad semantics and the composition of
the component instances. Composition is illustrated in
Figure~\ref{fig:simple} where the details of each component, since
they are not needed, are omitted. This type of property is also
offered by Aeolus~\cite{dicosmo2014ic}, albeit without parallelism
within components.

\begin{figure}[tbp]
  \begin{center}
    \includegraphics[width=0.6\linewidth]{./images/simpleass2.pdf}
  \end{center}
  \caption{\mad assembly of an Apache component and a MariaDB
    component without knowing the details of each component.}
  \label{fig:simple}
\end{figure}

The formalization of \mad will be detailed in
Section~\ref{sec:formal_model}.

%%%%%%%%%%%%%%%%%%%%%%%%%%
% \subsection{Meta-model}

% \HC[Maverick]{figures a refaire avec nos discussions}

% \begin{figure}[tbp]
%   \begin{center}
%     \includegraphics[width=0.9\linewidth]{./images/ass_uml.pdf}
%   \end{center}
%   \caption{\mad meta-model of an assembly, \ie an overall
%     commissioning procedure of a distributed software.}
%   \label{fig:mmass}
% \end{figure}

% Figures~\ref{fig:mmass} and~\ref{fig:mmcomp} respectively depict the
% UML diagram representing the \mad meta-model of an assembly, and the
% meta-model of a component. A \mad assembly is not different from a
% usual assembly in any component model of the literature. An assembly
% basically contains component instances, connected to each other
% through their compatible ports. One specificity is that \mad handles
% four types of ports instead of usually two, by dissociating service
% ports from data ports, each having their own semantics. Intuitively,
% unlike service ports, data ports act as data registries thus, once
% activated, they will remain active forever.

% \begin{figure}[tbp]
%   \begin{center}
%     \includegraphics[width=0.9\linewidth]{./images/component_uml.pdf}
%   \end{center}
%   \caption{\mad meta-model of a component, \ie the commissioning
%     procedure of a single component (\ie module or service).}
%   \label{fig:mmcomp}
% \end{figure}

% However, as already explained a \mad component is very different
% from usual components in the literature. A component type contains a
% set of places, a set of transitions, and a set of service-provide,
% service-use, data-provide and data-user ports.
% Service-provide ports (service or data) are bound to groups of
% places. Those groups of places represent the sub-part of the
% commissioning procedures where a given service is provided. A
% data-provide port is simply bound to one place from which the data is
% provided. Use ports (data or service) are bound to transitions where
% the corresponding data or services modeled by ports are actually used
% by the code executed by the transition.

%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Concrete language}

\mad also comes with a prototype and a concrete syntax that are
implemented in \python. \mad is a declarative language that follows
the model previously presented to define control component types and
assemblies of components. \python has been chosen to prototype \mad
because it is a widely known language within the DevOps
community. Furthermore, with \python, parametric components and
assemblies can easily be defined. Note however that another
implementation choice could have been to design a descriptive language
closer to TOSCA or Ansible, for instance using YAML.

Listing~\ref{codemdb} shows the declaration of the MariaDB component
type of Figure~\ref{fig:example}. Lines 3 to 8 declare the places of
the component type. A place is identified by a unique
\texttt{string}. Lines 9 to 15 declare the transitions of the
component type. A transition is identified by a unique key (a
\texttt{string}), and is associated through a dictionary with a source
and a destination place. Moreover, each transition is associated with
a function to call to perform corresponding actions. For instance, the
function \texttt{f\_pull} of transition \texttt{pull} is declared on
line 20, and provides the code to execute during this
transition. Finally lines 16 to 19 declare the coordination ports of
the control component type. A port is identified by a unique key (a
\texttt{string}), and is associated with a type (use or provide) and
the elements to which it is bound. As described previously, provide
ports are bound to sets of places, and use ports to a set of
transitions. In the case of MariaDB, one provide port, namely
\texttt{serv}, is provided from the place \texttt{std}, and
another (data) provide port, namely \texttt{ip} is provided from the
first place \texttt{wtg}.

\input{./code/mariadb.tex}

Similarly, Listing~\ref{codeapache} shows the declaration of the Apache control component of Figure~\ref{fig:example}. The Apache component type contains two use coordination ports, one modeling data and one modeling a service (lines 18 to 21).

\input{./code/apache.tex}

Finally, Listing~\ref{codeass} shows the declaration of the assembly of components of Figure~\ref{fig:example}. Lines 6 and 7 respectively instantiate the component types MariaDB and Apache previously declared. Line 9 to 13 perform the creation of an assembly, the addition of component instances to the assembly, and the connections of the components. Finally, lines 15 and 16 run the assembly to perform the commissioning of MariaDB/Apache. An overview of this execution is given in the next section.

\input{./code/assembly.tex}

In this work, we assume that the placement optimization problem is solved. Thus, there is no particular need for a way to specify the infrastructure on which the deployment will be performed. In our use cases, the placement information is given exactly as in Ansible, \ie using inventory files (Section~\ref{sec:related_work}).

%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Execution}

In \mad, executing a commissioning procedure requires the execution of an assembly. The \mad execution model is governed by operational semantics rules to move from one configuration to another. The concept of configuration, which is introduced formally in Section~\ref{sec:formal_model}, intuitively corresponds to a snapshot of the execution of an assembly. It is composed of the location of the tokens (modeling the evolution of the process), a history of places that have been reached, and the set of actions (transitions) under execution. In practice, semantics rules move tokens from places to transitions within components. Those rules are inspired by those of Petri nets, yet have a specific semantics for docks, transitions, bindings and ports. Details of the transformation from a \mad assembly to a Petri net are given in~\cite{coullon:hal-02323641}.

%% In this paper, the execution of a \mad assembly has been streamlined
%% compared to our previsous work~\cite{chardet:hal-01858150} and is
%% defined by five operational semantic rules. These rules are formally
%% specified in Section~\ref{sec:formal_model}. Note in
%% Figure~\ref{fig:example} the small squares that have not been
%% explained yet, namely \emph{docks}. Docks are attached to places and
%% divided in two groups, input and output docks. Input docks represent
%% synchronization points to enter a place, and output docks represent
%% scatering points to leave a place and start parallel
%% transitions. Thus, each transition has a source dock and a destination
%% dock. Docks have not been detailed previously because they are
%% automatically infered from the concrete syntax of \mad. However, this
%% concept is needed to explain the four semantics rules:
%
In the formal model, transitions are composed of three elements: an
output dock attached to the source place, an action, and an input dock
attached to the destination place. Docks are used to model
synchronization points in the execution: the output dock holds a token
if the action is ready to be started, whereas the input dock holds a
token after the action has ended.

In this paper, the execution model of \mad has been streamlined
compared to our previous work~\cite{chardet:hal-01858150}. It is
defined by five operational semantic rules, that are formally
specified in Section~\ref{sec:formal_model}. These rules correspond to
the following behaviors:

\begin{enumerate}
\item \emph{Reaching a place}: when all the actions required by a place have finished, the place can be reached. Note that the enabling of provide ports depends on place reached, so this event may enable additional ports.
\item \emph{Leaving a place}: after a place of the deployment has been reached, new actions can be started. Thus tokens are moved to output docks (\ie source docks of outgoing transitions).
\item \emph{Firing a transition}: if the source dock of a transition contains a token, and all use ports bound to this transition are enabled, the action associated with the transition may be initiated.
\item \emph{Terminating an action}: an ongoing action may terminate at any point during the execution. This captures the fact that, after an action has been initiated, its execution and termination are outside of the control of the \mad model. However, our notion of execution excludes non-terminating actions.
\item \emph{Ending a transition}: after an action terminates, a token is placed on the output dock of the corresponding transition.
\end{enumerate}

\begin{figure}[tbp]
  \begin{center}
    \includegraphics[width=0.7\linewidth]{./images/scenari.pdf}
  \end{center}
  \caption{Three possible intermediate configurations during the commissioning
  of the assembly presented in Figure~\ref{fig:example}. Each configuration is
  represented by a different color.}
  \label{fig:scenari}
\end{figure}

\paragraph{Example}{
Let us detail the commissioning execution of the Apache/MariaDB
example. The initial assembly, given in Figure~\ref{fig:example}, has
already been described. Figure~\ref{fig:scenari} depicts three examples of
configurations that may occur at different steps of the
commissioning. In chronological order, the first configuration is
represented by red tokens, the second one by blue tokens, and the
third one by green tokens.

In the first configuration, on the one hand, the red token of MariaDB
is found on the second place, which can only occur after a token has
been moved from the initial place of the component to the output dock
of transition \texttt{provision} and the action associated with this
transition has been executed. The provide port bound to this place
models data provided by the component, in this case the IP adress
resulting from the action of \texttt{provision}. As soon as this
second place is reached, the provide port \texttt{@IP mdb} is enabled,
and the use port connected to it is provided. On the other hand, the
initial token of Apache has been replaced by three tokens, one for
each output dock. Two of these tokens have been used to fire
respectively the transitions \texttt{bootstrap} and \texttt{pull}, and
\texttt{pull} has terminated. However, the third token has remained in
its dock. The transition \texttt{conf} could not be fired until the
bound use port became provided. In this configuration, this is the
case and the transition may be fired.

The blue configuration illustrates a case where parallel transitions
\texttt{pull}, \texttt{conf}, and \texttt{bootstrap} of MariaDB are
executed simultaneously.

Finally, the green configuration illustrates an example where MariaDB
reaches its penultimate place. The provide port \texttt{mdb} bound to
it models the service offered by the component. As soon as the place
holds a token, the provide port becomes enabled. As a result, the use
port of Apache that is connected to it becomes provided, and the
transition \texttt{check} of Apache can be fired.}
