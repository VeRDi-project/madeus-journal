In this section, we present a theoretical performance prediction model
for \mad that estimates the total execution time of the
commissioning of a given assembly, according to the execution time of
each transition. Three goals are pursued with such a model. First, the
theoretical execution time offers a way to evaluate the quality of the
prototype of \mad. Second, for sysadmins, devops or developers,
knowing the theoretical execution time of the commissioning procedure
is useful. Third, in the case of a fully autonomic commissioning and
reconfiguration process, which is left to future work, theoretical
information on the execution time is needed to take adequate
decisions, in particular for critical systems and services.

To build this performance prediction model, we need an estimation of
the execution time of all the actions in the assembly, given as a
function $t\,:\,\actions\rightarrow\mathbb{R}^+$.  Computing
precise execution times for the kind of actions used in a deployment
can be difficult and is itself the subject of many studies, \eg using
an analytical model or a statistical approach. However, we find that estimations are sufficient as long as they preserve the relative difference between transitions~\cite{coullon:hal-02323641}. 
%\CP{Cette phrase est un peu boiteuse car
%on ne sait pas ce que sont nos applications -- faire une ref à la bonne section??}
%
Intuitively, we automatically deduce the execution flow of a \mad
assembly based on \mad' formal semantics. This is done by generating a
dependency graph representing the execution flow of each \mad
component in the assembly and connecting them together according to their
dependencies (the connections between their coordination ports). Then, a
\emph{source} vertex is connected to the vertices representing the beginning of
the execution of each component, and a \emph{sink} vertex is connected to
the vertices representing the end of the execution of each component.
%
Thus, a dependency graph representing the execution of the whole assembly is obtained. By weighting the arcs corresponding to the transitions with the individual execution times of their actions (and the other ones with 0), we can compute the expected total execution time of the assembly as the weighted longest path from the \emph{source} vertex to the \emph{sink} vertex.

% By generating a graph modelling the execution flow of a \mad assembly,
% capturing both intra-component and inter-component dependencies, we
% reduce this problem to finding the longest path in a DAG.

\subsection{Assumptions}

The performance model given here is valid only for assemblies such that any set of places bound to a provide port contains exactly one place. Indeed, if a provide port is bound to a set containing multiple places, the requirement imposed by this binding is satisfied as soon as one of the places is reached. Thus, the total time then depends on the earliest time at which one of these places is reached, which cannot be modeled as a longest path. In practice this is not a severe restriction, as ports requirements very rarely include disjunctions. Indeed, none of our use cases rely on this feature. %However, we still consider the possibility of having multiple sets (of one place) bound to a port. In this case, the port is enabled when all the places are reached.

%% \MC{Préciser ici que dans la pratique ce n'est pas un problème ?}
%% \HC[Maverick et Simon]{oui, j'ai commencé une phrase, il faut donner
%%   l'intuition du problème posé par la disjonction et aussi dire que
%%   dans la pratique sur des cas réels nous n'avons jamais eu besoin de
%%   disjonctions.}

The estimation provided by the model is a best-case scenario: it assumes that all actions are started as soon as possible, and that the hardware has the capacity to run all possible parallel actions without affecting their execution times.

%--------------------------------------
\subsection{Notations}
%--------------------------------------

%% Recall that given a component, we denote its set of places $\Pi$ and its
%% set of transitions $\Theta$. In the following, we consider that the
%% transitions go directly from a place to
%% another place instead of from an output dock to an input dock.
%% Hence, $\Theta$ is a multiset which elements are pairs of places.
%% \MC[Maverick,Simon]{Si on change les transitions pour qu'elles contiennent l'action,
%% plus besoin de parler de multiset je pense (+TODO ce ne sont plus des paires)}
%We
%can obtain the place corresponding to each dock by using the $place$
%function of the component.

%% We also consider the following two functions:
%% \begin{itemize}
%% \item the \emph{group entrance function} $g_{in}\,:\,G\rightarrow\mathcal{P}\left(\Pi\right)$ with\\
%% $g_{in}(g)=\left\{ \pi\,\mid\,\pi\in g\land\exists\pi_{b}\,:\,\left(\pi_{b}\not\in g\land\left(\pi_{b},\pi\right)\in\Theta\right)\right\} $
%% (the result of $g_{in}$ is called the set of \emph{entrance places}
%% of the group)
%% \item the \emph{group exit function} $g_{out}\,:\,G\rightarrow\mathcal{P}\left(\Pi\right)$ with\\
%% $g_{out}(g)=\left\{ \pi\,\mid\,\pi\in g\land\exists\pi_{a}\,:\,\left(\pi_{a}\not\in g\land\left(\pi,\pi_{a}\right)\in\Theta\right)\right\} $
%% (the result of $g_{out}$ is called the set of \emph{exit places}
%% of the group)
%% \end{itemize}

Recall that an assembly is a tuple $A = (C,L)$. In the following, for
any component $c_i \in C$, any of its associated set is denoted
$X_i$, \eg $\Pi_i$ for the set of places of the component $c_i$.  As
previously, for each set $X$ defining a component, we denote $X^*$ the
union (in the case of an assembly) or the extension (in the case of a
function) for all components. For instance
$\Pi^*=\bigcup_{i=1}^{n}\Pi_i$ denotes the set of all places in the
assembly.

The execution flow graph is an oriented weighted graph $\left(V,E\right)$
where $V$ is the set of vertices and $E$ is the set of weighted
arcs with elements in $V\times \mathbb{R}^{+} \times V$. We define
$V$ and $E$ in the following.

%--------------------------------------
\subsection{Vertices}
%--------------------------------------

For each place, we introduce one vertex that represents the event of
the place being reached.
\[
V_{\Pi}=\bigcup_{\pi\in\Pi^*}\left\{ v_\pi^\text{reach}\right\} 
\]

%% \MC[Maverick]{TODO : vérifier si on peut fusionner les deux sommets de
%%   place}
%% \HC[Maverick]{si c'est long a verifier, laisser comme ça, il faut
%%   qu'on soumette !}
%% SR: fait

For each transition, we introduce a vertex representing its firing.
\[
V_{\Theta}=\bigcup_{\theta\in\Theta^*}\left\{ v_\theta^\text{fire}\right\} 
\]

%% \SR{On peut sans doute retirer les vertices representant les ports, et
%%   directement connecter $v^{place}_\pi$ à $v^{beg}_\theta$.}
%% \HC[Maverick et Simon]{oui je pense aussi, mais même remarque que au
%%   dessus, il faut qu'on soumette !}
%% \SR{Réponse de Maverick: la façon de faire actuelle permet d'éviter un
%%   nombre quadratique d'arcs pour les ports liés à plusieurs transition
%%   et ensembles de places.}

For each provide port we introduce one vertex representing its
enabling.
\[
V_{S_p}=\bigcup_{p\in S_p^*} \left\{ v_p^\text{enable}\right\} 
\]

Finally, we define $V$ as the union of all these, plus one source
and one sink vertices. 
\[
V=V_{\Pi}\cup V_{\Theta} \cup V_{S_p} \cup \left\{ v^\text{source},v^\text{sink}\right\} 
\]

%--------------------------------------
\subsection{Arcs}
%--------------------------------------

In the dependency graph $\left(V,E\right)$, arcs represent time constraints: the event represented by the destination vertex must happen after the one represented by the source vertex, at least $w$ seconds apart where $w$ is the weight of the arc. In practice, arcs corresponding to actions are weighed with the corresponding time, while other arcs have a weight 0 and merely represent a dependency.

%% For each place $\pi$ we introduce one arc from $v_\pi^\text{place}$ to
%% $v_\pi^\text{leav}$. This represents the fact that a token may leave $\pi$
%% only after it has entered it.
%% Figure~\ref{fig:place_graph} depicts the dependency graph corresponding to a place.
%% \[
%% E_{\Pi}=\bigcup_{\pi\in\Pi^*}\left\{ \left(v_\pi^\text{place},0,v_\pi^\text{leav}\right)\right\} 
%% \]

%%%%%%%%%%%%%%%
%\input{figures/placetodag.tex}

For each transition $\theta = (s, \alpha, d)$, we introduce
two arcs.
%
The first, from $v_{\pi(s)}^\text{reach}$
to $v_\theta^\text{fire}$, represents the fact that $\theta$ may only
be fired after $\pi(s)$ has been reached.
%
The second, from $v_\theta^\text{fire}$ to $v_{\pi(d)}^\text{reach}$
represents the fact that a token may enter $\pi(d)$ only after
$\theta$ has ended, which requires a time $t(\alpha)$ corresponding
to the action.
\[
E_\Theta = \bigcup_{\theta=(s,\alpha,d)\in\Theta^*} \left\{ \left(v_{\pi(s)}^\text{reach},0,v_\theta^\text{fire}\right), \left(v_\theta^\text{fire},t(\alpha),v_{\pi(d)}^\text{reach}\right)\right\} 
\]


Figure~\ref{fig:transition_graph} depicts the dependency graph corresponding to an example made of three transitions \emph{t1}, \emph{t2} and \emph{t3}.

%%%%%%%%%%%%%%%
\input{figures/transtodag.tex}

%% SR: text before modification
%% For each data-provide port $p$, we associate (a) for each binding between
%% $p$ and a place $\pi$ one arc from $v_\pi^\text{place}$ to
%% $v_p^\text{enable}$, and (b) for each connection connecting $p$
%% to a data-use port $u$, for each binding between $u$ and a transition
%% $\theta$ one arc from $v_p^\text{enable}$ to $v_\theta^\text{fire}$.

For each provide port $p$ and each place $\pi$ such that
$\{\pi\}$ is bound to $p$, we introduce an arc from
$v_\pi^\text{reach}$ to $v_p^\text{enable}$. This represents the
enabling of the port after all (sets of) places bound to the port
have been reached.
\[
E_{S_p}= \bigcup_{(p,\{\pi\})\in B_{S_p}^*}\left\{ \left(v_\pi^\text{reach},0,v_p^\text{enable}\right)\right\}
\]

Additionally, for each provide port $p$ and each transition
$\theta$ such that $p$ is connected to a use port that is bound
to $\theta$, we introduce an arc from $v_p^\text{enable}$ to
$v_\theta^\text{fire}$. This represents the activation of the transitions
bound to a use port, after the port is provided.
\[
E_{S_u} = \bigcup_{\substack{(u,p)\in L \\ (u,\theta)\in B_{S_u}^*}} \left\{ \left(v_p^\text{enable},0,v_\theta^\text{fire}\right)\right\}
\]

Figure~\ref{fig:ports_graph} depicts the dependency graph corresponding to a
provide port \emph{p} bound to a place \emph{c1p} and connected to
a use port \emph{u} which is bound to a transition \emph{c2t}.
%\SR[Maverick]{TODO: change figure (part a)}

%% SR: text before modification
%% Figure~\ref{fig:data_ports_graph} depicts the dependency graph corresponding to a
%% data-provide port \emph{dp} bound to a place \emph{c1p} and connected to
%% a data-use port \emph{du} which is bound to a transition \emph{c2t}.
%% \begin{align*}
%% A_{D}=\bigcup_{p\in D_p}
%% & \left(\bigcup_{\pi,\,\left(p,\pi\right)\in B_{D_{p}}^*}\left\{ \left(v_\pi^\text{place},v_p^\text{enable},0\right)\right\}\right. \\
%% & \left.\cup\bigcup_{\substack{u,\,\left(u,p\right)\in L_D \\
%%   \theta,\,\left(u,\theta\right)\in B_{D_{u}}^*}}\left\{ \left(v_p^\text{enable},v_\theta^\text{fire},0\right)\right\}\right)
%% \end{align*}

%%%%%%%%%%%%%%%
\input{figures/porttodag.tex}

%% SR: text before modification (see below) 
%% For each service-provide port $p$, we associate (a) for each binding between
%% $p$ and a group $g$ one arc from $v_\pi^\text{place}$ to $v_p^\text{enable}$
%% for each $\pi$ in $g_{in}(g)$ and one arc from $v_p^\text{stop}$ to
%% $v_\pi^\text{leav}$ for each place $\pi$ $g_{out}(g)$, and (b) for each
%% connection connecting $p$ to a service-use port $u$, for each binding between
%% $u$ and a transition $\theta$ one arc from $v_p^\text{enable}$ to
%% $v_\theta^\text{fire}$ and one arc from $v_\theta^\text{end}$ to
%% $v_p^\text{stop}$.
%% The (a) arcs represent the fact that the service becomes available when all
%% groups bound to it have a token, and each of them can be deactivated only when
%% the provide port is not used anymore. The (b) arcs represent the
%% fact that the transitions bound to a service-use port may only execute once
%% the port is provided, and do not use the port anymore once they are over.

%% For each service-provide port $p$, each groupd $g$ bound to $p$ and
%% each place $\pi$ in $g_{in}(g)$, we introduce one arc from
%% $v_\pi^\text{place}$ to $v_p^\text{enable}$, representing the
%% availability of the service once all groups bound to it hold a token.

%% Conversely, for each each service-provide port $p$, each group $g$
%% bound to $p$ and each place $\pi$ in $g_{out}(g)$, we introduce one
%% arc from $v_p^\text{stop}$ to $v_\pi^\text{leav}$, in order to
%% represent the fact that the group $g$ may not be disabled while the
%% port is busy.

%% Lastly, for each service-provide port $p$ and each transition $\theta$
%% such that $p$ is connected to a service-use port bound to $\theta$, we
%% introduce one arc from $v_p^\text{enable}$ to $v_\theta^\text{fire}$ and
%% one arc from $v_\theta^\text{end}$ to $v_p^\text{stop}$. They
%% represent the activation of the transitions bound to a service-use
%% port after it has been provided, and the release of the port after the
%% transition is over.

%% Figure~\ref{fig:service_ports_graph} depicts the dependency graph corresponding to a
%% service-provide port \emph{sp} bound to a group with \emph{c1p1} as entrance
%% place and \emph{c1p2} as exit place, and connected to service-use port
%% \emph{su} which is bound to transition \emph{c2t}.

%% \begin{alignat*}{3}
%% A_{S}=\bigcup_{p\in S_p}
%% & \left(\bigcup_{g,\,\left(p,g\right)\in B_{S_{p}}^*} \right. && \left. \left(\bigcup_{\pi\in g_{in}(g)} \left\{\left(v_\pi^\text{place},v_p^\text{enable},0\right)\right\}\right.\right. \\
%% &&& \left. \cup\left.\bigcup_{\pi\in g_{out}(g)} \left\{\left(v_p^\text{stop},v_\pi^\text{leav},0\right)\right\}\right)\right. \\
%% & \left.\cup\bigcup_{\substack{u,\,\left(u,p\right)\in L_S \\
%%       \theta,\,\left(u,\theta\right)\in B_{S_{u}}^*}} \right. && \left. \left\{ \left(v_p^\text{enable},v_\theta^\text{fire},0\right),\right.\right. \left.\left.\left(v_\theta^\text{end},v_p^\text{stop},0\right)\right.\Big\}\right.\Bigg)
%% \end{alignat*}

%%%%%%%%%%%%%%%
%%\input{figures/servporttodag.tex}

For each initial place $\pi$, we introduce an arc from $v^\text{source}$
to $v_\pi^\text{reach}$, representing the fact that a token is placed in each
initial place in the initial configuration.
%% Figure~\ref{fig:source_sink_graph} depicts the dependency graph corresponding to two initial
%% places \emph{c1p1} and \emph{c2p2}.
\[
E_I=\bigcup_{\pi, \Delta_i(\pi) = \emptyset}\left\{ \left(v^\text{source},0,v_\pi^\text{reach}\right)\right\} 
\]

For each final place $\pi$, we introduce an arc from $v_\pi^\text{reach}$
to $v^\text{sink}$. Intuitively, this represents the fact that the
commissionning is over only after all components have reached the
places without outgoing transitions.
\[
E_F=\bigcup_{\pi, \Delta_o(\pi) = \emptyset}\left\{ \left(v_\pi^\text{reach},0,v^\text{sink}\right)\right\} 
\]

Finally, we define $E$ as 
\[
E= E_{\Theta} \cup E_{S_p}\cup E_{S_u}\cup E_{I}\cup E_{F}
\]

Figure~\ref{fig:source_sink_graph} depicts a complete example with a \mad assembly and its associated dependency graph.
%\SR{Ce serait bien d'avoir un port dans l'exemple}

%%%%%%%%%%%%%%%
\input{figures/exampletodag.tex}

%--------------------------------------
\subsection{Time estimation}
%--------------------------------------

In the following, we denote $DG_A=(V,E)$ the dependency graph corresponding to an assembly $A=(C,L)$. We define the time estimation of the execution of the \mad assembly $A$ to be the length of the waighted longest path from $v^\text{source}$ to $v^\text{sink}$ in $DG_A$. Clearly, the size of $DG_A$ is linear in the size of $A$. Furthermore, the graph is either a DAG or has a path of infinite length, so the longest path can be computed in polynomial time.

Since our execution model does not include a notion of time, proving the correctness of this performance model is outside the scope of this paper. However we show that this performance model can be used to assess reachability.

\begin{lemma}\label{thm:reachedass}
  Let $A$ be an assembly of well-formed components. If the longest
  path from $v^\text{source}$ to $v^\text{sink}$ in $DG_A$ has a
  finite length, then in any execution of $A$, all places in $A$ are
  eventually reached.
\end{lemma}

\begin{proof}
  By construction of $DG_A$, there exists no cycle formed only of arcs
  of weight 0. Together with the assumption that there are no paths of infinite length from $v^\text{source}$ to $v^\text{sink}$, this implies that no path from $v^\text{source}$ to $v^\text{sink}$ features a cycle. Since every vertex
  in $DG_A$ is located on some path from $v^\text{source}$ to
  $v^\text{sink}$, we have that $DG_A$ is acyclic.

  Then we can use well-founded induction on the structure of the graph
  to verify that, for any vertex $v$, if $v = v^\text{reach}_\pi$ for
  some place $\pi$, then $\pi$ is eventually reached in any execution
  of $A$. The proof is similar to that of
  Lemma~\ref{thm:reachedplaces}.
\end{proof}

To sum-up, from a given assembly without disjonctions on provide ports can be built an execution flow graph. If this graph does not contain infinite paths, which is easy to verify, and by Lemma~\ref{thm:reachedass}, the reachability of the deployement modeled by the assembly is verified. The disjonction case is time dependent and creates non-determinism. The reachability of such complex cases should be verified by other techniques such as Model-checking~\cite{coullon:hal-02323641}.

%% \begin{lemma}
%%  In $DG_A$, if $v^\text{sink}$ is reachable from $v^\text{source}$ and there
%%  are no cycles, then the time estimation for $A$ is well-defined.
%%  \label{lemma:well_defined}
%% \end{lemma}

%% \begin{proof}
%%  If $v^\text{sink}$ is reachable from $v^\text{source}$, there exists at least
%%  one path from $v^\text{source}$ to  $v^\text{sink}$. Because there are no
%%  cycles, the number of paths between $v^\text{source}$ and $v^\text{sink}$ is
%%  finite. Because all the paths have a weight, the set of longest paths is
%%  well-defined (and not empty). Therefore the length of a longest path is
%%  well-defined.
%% \end{proof}

%% \begin{lemma}\label{lemma:reachable}
%%  In a component $c \in C$, if $\pi_t$ depends on $\pi_s$, then there
%%  is a path from $v_{\pi_s}^\text{reach}$ to $v_{\pi_t}^\text{reach}$ in $DG_c$.
%% \end{lemma}

%% \begin{proof}
%%  By assumption, there exists a sequence $(\pi_1, \pi_2, \dots, \pi_n)$
%%  such that $\pi_1 = \pi_s$ and $\pi_n = \pi_t$ and $\pi_i$ precedes
%%  $\pi_{i+1}$ for $1 \le i < n$. Therefore, by definition of
%%  $E_\Theta$, there is a path from $v_{\pi_i}^\text{reach}$ to
%%  $v_{\pi_{i+1}}^\text{reach}$ for $1 \le i < n$, hence a path from
%%  $v_{\pi_s}^\text{reach}$ to $v_{\pi_t}^\text{reach}$
%% \end{proof}


%% \begin{lemma}
%%  If an assembly $A$ has one or more components then $v^\text{sink}$ is
%%  reachable from $v^\text{source}$ in $DG_A$.
%%  \label{lemma:source_sink}
%% \end{lemma}

%% \begin{proof}
%%  Let us consider one component $c \in C$, its initial place $\pi_i$ and one of
%%  its final places $\pi_f$. By construction, $v_{\pi_i}^\text{reach}$ is
%%  reachable from $v^\text{source}$, and $v^\text{sink}$ is reachable from
%%  $v_{\pi_f}^\text{reach}$ in the dependency graph of an assembly containing this
%%  component. The last thing to prove is that $v_{\pi_f}^\text{reach}$ is reachable
%%  from $v_{\pi_i}^\text{reach}$ in $DG_c$.
%%  The end of the execution of a \mad component is defined as
%%  when all its tokens are in final places, \ie when there are no more
%%  transitions to perform. Therefore, by definition $\pi_f$ is
%%  necessarily reachable from $\pi_i$ in the \net of $c \in C$.
%%  We conclude by applying Lemma~\ref{lemma:reachable}.
%% \end{proof}

%% \begin{lemma}
%%  If a component $c \in C$ is well-formed then $DG_c$ has no cycle.
%%  \label{lemma:no_cycles_component}
%% \end{lemma}

%% \begin{proof}
%%  By construction, in $DG_c$ the vertices and arcs corresponding to places do
%%  not form cycles and are disjoint, so they cannot produce a cycle by themselves.
%%  Likewise, the arcs corresponding to port bindings have either their source
%%  vertex or their destination vertex of degree 1, so they cannot produce a cycle.
%%  Only the arcs corresponding to transitions can produce cycles. Moreover, the
%%  arcs in $DG_c$ connect only the vertices corresponding to the places which
%%  are connected in the \net. This means that if there is a cycle in $DG_c$,
%%  there is a cycle in the \net. However, the \net of a well-formed \mad component
%%  has no cycle. Therefore there are no cycles in $DG_c$.
%% \end{proof}

%% \begin{lemma}
%%  In an assembly $A$ of well-formed components with no deadlocks, if each port
%%  is bound to at most one element (place, transition or group) and if
%%  $\left|g_{in}(g)\right|\leq 1$ and $\left|g_{out}(g)\right|\leq 1$ for each group
%%  $g$ in $A$ then the $DG_A$ has no cycles.
%%  \label{lemma:no_cycles_assembly}
%% \end{lemma}

%% \begin{proof}
%%  By Lemma~\ref{lemma:no_cycles_component}, we have that the dependency graph
%%  of all the components in $A$ have no cycles.
%%  Then, the only way a cycle may exist in $DG_A$ is if the vertices and arcs
%%  corresponding to connections cause a cycle to exist.
%%  This may be acheived in two ways: because of a single service port bound to
%%  group $g$ if $\left|g_{out}(g)\right|>0$ (which implies by hypothesis
%%  $\left|g_{out}(g)\right|=1$); or because of two or more use and provide ports.
%%  First, because $\left|g_{out}(g)\right|=1$ (we consider $\pi_{out}$ to be the only
%%  place in $g_{out}(g)$), and because by construction $v_{\pi_{out}}^\text{reach}$ is
%%  reachable from $v_{\pi_{in}}^\text{reach}$ (where $\pi_{in}$ is the only place in $g_{in}(g)$,
%%  then $v_{\pi_{in}}^\text{reach}$ is not reachable from $v_{\pi_{out}}^\text{reach}$ (otherwise
%%  there would be a cycle in the dependency graph of the component). Second, a
%%  cycle can be caused by multiple connections if there are \emph{crossing
%%  dependencies} (see Figure~\ref{fig:deadlock}). However, this would create a
%%  deadlock in the assembly, which is not possible by hypothesis.
%% \end{proof}

%% \begin{figure}[t]
%%   \begin{center}
%%     \includegraphics[width=0.5\linewidth]{./images/deadlock.pdf}
%%   \end{center}
%%   \caption{Example of an invalid assembly: crossing dependencies cause a deadlock.}
%%   \label{fig:deadlock}
%% \end{figure}

%% \begin{theorem}
%%  In an assembly $A$ of one or more well-formed components with no deadlocks,
%%  if each port is bound to at most one element (place, transition or group) and if
%%  $\left|g_{in}(g)\right|\leq 1$ and $\left|g_{out}(g)\right|\leq 1$ for each group
%%  $g$ in $A$, then the time estimation obtained using $DG_A$ is well-defined.
%%  \label{theorem:well_defined}
%% \end{theorem}

%% \begin{proof}
%%  By applying Lemma~\ref{lemma:source_sink}, we know that $v^\text{sink}$ is reachable
%%  from $v^\text{source}$. By applying Lemma~\ref{lemma:no_cycles_assembly}, we know
%%  that there are no cycles in $DG_A$. We conclude by applying
%%  Lemma~\ref{lemma:well_defined}.
%% \end{proof}

%% \paragraph{Remark}

%% The restrictions imposed in \ref{theorem:well_defined} are here to consider only
%% assemblies for which the structure of the dependency graph does not change
%% depending on the duration of the transitions. To handle arbitrary assemblies,
%% a more complex performance model is needed, which is left as future work. In
%% practice, we expect most real-life assemblies to fulfill these requirements.
%% In particular, all of the assemblies presented in this article do.

%% \subsection{Complexity}

%% We now determine the size complexity of $DG_A=(V,E)$ and the time
%% complexity of computing the time estimation.

%% First, we notice that
%% $|V| \in \mathcal{O}\left(\left|\Pi^*\right|+\left|\Theta^*\right|+\left|S_p^*\right|\right)$
%% and
%% $|E| \in \mathcal{O}\left(\left|\Pi^*\right|+\left|\Theta^*\right|+\left|B_{S_p}^*\right|+\left|B_{S_u}^*\right|\times\left|L\right|\right)$
%% .

%% Because $DG_A$ is a a directed acyclic graph, finding the longest path
%% between $v^\text{source}$ and $v^\text{sink}$, \ie finding a time estimation,
%% can be done in $\mathcal{O}(|V|+|E|)$ by sorting the
%% vertices topologically and iterating through them, computing their maximum
%% distance from the source using the one of their parents.
