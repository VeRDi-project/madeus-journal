% 1. introduce metrics of RW
% - soft. eng., composition, code reuse, sep of concerns
% - parallelism levels
% 2. RW analyze
% - each tool
% - table
% 3. discussion
% - intra-task
% - performance model


%\HC[Helene]{encore trop verbeux, si vous voyez des simplifications possibles merci 
%de l'annoter}

In this section we give an overview of existing solutions for the
automation of distributed software commissioning. Then, we select the
most relevant solutions for deeper analysis, and we compare them
according to a specific set of metrics.

%----------------------------
\subsection{Automation of distributed software commissioning}
\label{sec:rwclasses}
%----------------------------
% 1. scripting
% 2. infrastructure as code
% 3. provisioning tools
% 4. orchestration tools
% 5. scope of the paper
% - software commissioning of a single distributed software
% - generic solution that could use or not provisioning tools
% - scripting/IaC but keep Kubernetes and Juju

We consider five classes of tools and languages to automate
commissioning procedures. In practice, these tools are often used in
combination.

Languages such as \bash or \ruby are commonly used to automate
commissioning procedures. They are very flexible, and well known among
system administrators and operators. However, in terms of software
engineering, they suffer from many limitations, including poor
separation of concerns, limited compositionality and complexity of
coordination mechanisms.

\paragraph{Software configuration tools}
This class contains tools such as \ansible~\cite{ansible:web},
\puppet~\cite{puppet:web}, \chef~\cite{chef:web},
\salt~\cite{salt:web}, and some academic contributions such as
\smartfrog~\cite{10.1145/1496909.1496915},
\engage~\cite{DBLP:conf/pldi/FischerME12}
\deployware~\cite{flissi2008ccgrid},
\aeolus~\cite{dicosmo2014ic,dicosmo:hal-01233489}. Their goal is to
enhance productivity when defining the deployment of components or
services, and the coordination between their tasks. They typically add
an abstraction layer on top of scripting languages, thus hiding some
technical details (\eg SSH connections), but may use different methods
to achieve their goals. For instance, Ansible adopts a procedural
imperative style in YAML, with a series of tasks to execute, while
Puppet adopts a declarative approach in Python, which combines
different instances of resources (\ie services, packages, etc.). In a
declarative approach, the user describes what is needed rather than how
to get it, which is instead determined by the tool. This approach
increases productivity at the expense of flexibility.

\paragraph{Infrastructure definition tools}
Also known as \emph{provisioning tools}, these tools are specifically
designed to handle complex distributed infrastructures composed of
multiple clusters and machines shared between users. Managing such
infrastructures is very difficult and error-prone, as each application
and each user may have different requirements, operating systems,
package versions, etc. Virtualization has been introduced to solve
this issue (in addition to improving portability, isolation between
users, etc.). Tools such as \docker~\cite{docker:web},
\terraform~\cite{terraform:web}, \juju~\cite{juju:web},
\cloudformation~\cite{cloudf:web} or \heat~\cite{heat:web} use
virtualization mechanisms (\eg virtual machines, containers) to reduce
the commissioning process to a set of commands that deploy a virtual
resource on a physical machine, by using an image. If a suitable image
is available, this reduces the complexity of the
commissioning procedure. Otherwise, the image must be created by
executing the commissioning commands. Some of these tools target a given virtualization
technique or cloud provider (\eg \docker, \cloudformation and
\heat), while others offer \emph{providers} for different
cloud infrastructures (\eg \terraform, \juju). The \tosca
standard~\cite{tosca:web}, and its
implementations~\cite{Binz2013,cloudify:web,opentosca:web,8599581} can
be classified as a descriptive provisioning tool.
% More precisely, \tosca is presented as a Cloud Modeling Language (CML).
%By using
%\tosca, a set of cloud-based services is modeled as a graph where each
%node represents a component of the application and each edge
%represents the relationship between components. \tosca allows for
%portability and automated management across cloud platforms and
%infrastructures. In \tosca, commissioning actions are scripts
%encapsulated in \emph{artifacts}.

\paragraph{Orchestration tools}
Recent tools such as \kubernetes~\cite{kubernetes:web} and
\dockerswarm~\cite{dockerswarm:web} go further, offering an
\emph{orchestration} level to handle shared clusters of machines
running many services simultaneously. In this case, deploying or
installing distributed software is only part of the problem, as
services also need to be restarted after failures, scaled to avoid
overload, etc. These tools are outside the scope of this paper, but a
subdivision of their architecture handles commissioning. For instance,
\kubernetes relies on \docker for deployments.

\paragraph{The specific class of CBSE}
Component-based software engineering (CBSE) focuses on (distributed)
software implementation and improves code re-use, separation of
concerns, and composability (thus
maintainability)~\cite{Szyperski:2002:CSB:515228}. A component-based
application, called an \emph{assembly}, includes a set of component
instances connected together. Each component is a black box that
implements a functionality of the application, and interacts with
other components through \emph{ports} that are used to decouple the
component internals from its interface. For instance, a port can be
used to declare that the component either provides a service ---in
this case the port is attached to an internal method--- or uses a
service from another component. Many component models focus on the
implementation of functionalities and
interfaces~\cite{corba:omg06,Blair2009,baude:hal-01001043,Bernholdt01052006,bigot:inria-00388508,Coullon2017}
of components, rather than on their commissioning. In the Object
Management Group's (OMG) specification~\cite{ccmdeploy:omg06}, the
commissioning model is rigid and fixed by the model. However, a few
component models have considered commissioning issues~\cite{Blair2009,
  baude:hal-01001043, flissi2008ccgrid, chardet:hal-01858150,
  dicosmo2014ic}. These CBSE solutions to deployment automation can be
categorized in some of the four previous classes.

%----------------------------
\subsection{Related work metrics}
%----------------------------
% 1. software engineering
% - components / service / module
% - sub-elements / tasks
% - separation of concerns when assembling 2 components, no need to
% know what each component does to commission itself
% 2. parallelism
% - node / SIMH
% - inter-component
% - inter-task
% - intra-task
% 3. formalism
% - formal model

Our analysis of the related work is based on several metrics that
measure: (i) software engineering (SE) aspects, (ii) level of
parallelism, and (iii) formal aspects. For each metric, we define four
levels, presented in Table~\ref{tab:comparison}: (1) supported, denoted
\checkmark and counting for 3 points in the score; (2) partially
supported, denoted (\checkmark) and counting for 2 points; (3)
manually supported, denoted \emph{M}, indicating a feature that can be
manually coded by the user, and counting for 1 point; and finally
(4) not supported, denoted -.

\paragraph{Software engineering}
Software engineering properties are of prime importance when
automating distributed software commissioning. Indeed, when several
actors are involved in one given goal, software engineering techniques
can help improve separation of concerns, meaning that each actor is
responsible for her own expertise domain. This is improved by modular
or component-based approaches, as each actor can be responsible for
one component. Furthermore, component-based architectures also feature
composition mechanisms that facilitate interactions between entities
implemented by different actors. Additional actors may also be
responsible for the composition. We define three SE metrics:
\begin{itemize}
\item \emph{component}: whether the solution (tool, framework or scientific
  contribution) offers a component-oriented (\eg services, modules)
  structure;
\item \emph{tasks}: whether the solution offers a way to divide the
  commissioning of each component in sub-elements that we call
  \emph{tasks}, for a better structured procedural design;
\item \emph{separation of concerns}: whether the solution offers a way to
  build distributed software commissioning by composing components
  without the need to know their internal behaviors.
\end{itemize}

\paragraph{Parallelism level}
A second important aspect of distributed software commissioning that
has been introduced in the motivations is its efficiency and in
particular (in this paper) the level of parallelism offered. We
consider four metrics:
\begin{itemize}
\item \emph{SIMH}: whether the solution offers a transparent way to
  perform the same instruction or set of instructions on multiple
  hosts simultaneously, when no dependencies exist between them;
\item \emph{inter-comp}: whether the solution offers a transparent way
  to simultaneously execute the commissioning of multiple components
  when no dependency exists between those;
\item \emph{inter-comp-tasks}: whether the solution offers a
  transparent way to simultaneously execute the commissioning tasks of
  multiple components until a dependency between tasks is reached;
\item \emph{intra-comp-tasks}: whether the solution offers a way to
  simultaneously execute two commissioning tasks of a given component,
  in other words whether a partial order on tasks can be given for a
  component.
\end{itemize}
The level of parallelism offered by a given tool is correlated to the
type of dependencies that can be declared. For instance, without any
dependency mechanism, only the SIMH level is achievable.%, while
%\emph{inter-comp} or \emph{inter-comp-tasks} dependencies allow more
%parallelism between components of different types.

\revised{Figure~\ref{fig:parlevels} illustrates the four parallelism levels
listed above through an example with three components or modules deployed on four nodes (or hosts):
component $C$ is deployed on two different nodes (namely \emph{Node 1} and \emph{Node 2}) while components $A$ and $B$ are deployed on a single node (namely \emph{Node X} and \emph{Node Y}, where $X$ and $Y$ can take any value and could even be equal). In this example, components $A$ and $B$ need
to be coordinated. This is represented by the dashed arrow between $A$ and $B$. Component $C$ is independent from both $A$ and $B$. The deployment procedure of each component is a sequence of tasks depicted as plain arrows, each identified by a label. Sub-figure~\ref{fig:simh} illustrates the \emph{SIMH} level,
where parallelism can be introduced for component
$C$.} Sub-figure~\ref{fig:inter-comp} illustrates the \emph{inter-comp}
parallelism level: as $C$ is independent from $B$, both commissionings
can be performed simultaneously. However, $B$ depends on $A$, and
because the dependencies are only available at the component level,
their commissionings have to be performed
sequentially. Sub-figure~\ref{fig:inter-comp-tasks} shows the
\emph{inter-comp-tasks} parallelism level, where dependencies can be
defined at the finer level of tasks. In this case, the commissionings
of $A$ and $B$ can be started in parallel until they reach their
dependencies. Thus, $B$ has to wait until $A$ performs needed tasks
before continuing its deployment. Finally,
sub-figure~\ref{fig:intra-comp-tasks} illustrates the
\emph{intra-comp-tasks} parallelism level, where some tasks inside a
component can be performed in parallel. Increased support in
parallelism leads to a shorter expected commissioning time, as
represented by the height of the figures.


\begin{figure*}[t!]
  \begin{center}
    \subfloat[SIMH]{
      \fcolorbox{black!20}{white}{
        \begin{minipage}[c][7cm][c]{0.45\linewidth}%
          \centering
          \scalebox{0.4}{\input{./figures/simh.tex}}
        \end{minipage}
      }\label{fig:simh}
    }
    \subfloat[SIMH + inter-comp]{
      \fcolorbox{black!20}{white}{
        \begin{minipage}[c][7cm][c]{0.45\linewidth}%
          \centering
          \scalebox{0.44}{\input{./figures/inter-comp.tex}}
        \end{minipage}
      } \label{fig:inter-comp}
    }
    \\

    \subfloat[SIMH + inter-comp + inter-comp-tasks]{
      \fcolorbox{black!20}{white}{
        \begin{minipage}[c][4.5cm][c]{0.45\linewidth}%
          \centering
          \scalebox{0.44}{\input{./figures/inter-comp-tasks.tex}}
        \end{minipage}
      }\label{fig:inter-comp-tasks}
    }
    \subfloat[SIMH + inter-comp + inter-comp-tasks + intra-comp-tasks]{
      \fcolorbox{black!20}{white}{
        \begin{minipage}[c][4.5cm][c]{0.45\linewidth}%
          \centering
          \scalebox{0.44}{\input{./figures/intra-comp-tasks.tex}}
        \end{minipage}
      } \label{fig:intra-comp-tasks}
    }
    \caption{Examples to illustrate the four parallelism levels
      considered in this paper. \revised{Plain arrows represent tasks to perform during deployment, their length corresponding to their duration. Dashed arrows represent dependencies or coordination between components or modules. Finer specification of dependencies leads to a higher level of parallelism, and consequently to a shorter deployment time.}}
    \label{fig:parlevels}
  \end{center}
\end{figure*}

%\SR{Give a reason to exclude declarative approaches, \eg worse/hard to measure performance}

Metrics for SE and parallelism are suitable for procedural approaches,
as they consider \emph{tasks} and not \emph{resources}. This excludes
\puppet or \salt, for instance. While declarative approaches have great
advantages, they are not considered in this paper. Indeed we put a strong emphasis on parallelism in this paper. This requires a fine-grain definition of deployments, at the level of tasks. Declarative approaches on the contrary raise the abstraction level at the price of flexibility.

\paragraph{Formalism}
The metric considered here is the existence of a formal model for each
commissioning solution. Indeed, we claim that formal study of
commissioning models and of their semantics is required for
verification and safety in deployments. For instance, the formal model
of \mad has been successfully used to verify safety properties on
distributed software commissioning by model checking, as presented
in~\cite{coullon:hal-02323641}.

%----------------------------
\subsection{Description and comparison of the related work}
% ----------------------------

We have selected 8 distributed software commissioning solutions for a
deeper comparison, including production tools (in particular those
with a significant open source community) and academic solutions.
%
%% The selection is made of 8 works selected from the five classes
%% introduced in Section~\ref{sec:rwclasses}: \shell, \fractal, \ansible,
%% \deployware, \aeolus, \juju, \tosca and \kubernetes.

% Shell-scripts
\paragraph{Shell scripts}
Traditionally, operators automate software commissioning by
transcribing actions and configurations from README files and
tutorials into a sequence of commands in shell scripts. On the one
hand, those scripts are written with low-level imperative languages,
and with good programming skills, it is possible to express complex
workflows (\eg idempotency, parallelism, remote actions using
SSH). For instance, parallelism can be managed by combining command
execution in the background (\eg using the control operator \& in
\bash) and synchronization commands such as \texttt{wait}. On the
other hand, as the system grows, any custom script becomes
error-prone, unpredictable, hard to understand and to maintain. Shell
scripts lack software engineering aspects and offer no framework to
express modules or tasks and their dependencies, thus hindering
separation of concerns. Table~\ref{tab:comparison} indicates that the
features corresponding to our metrics can be implemented manually with
shell scripts. Of course, this implementation is difficult, error-prone, and
time-consuming.
% devstack is a set of shell script to deploy OpenStack on a single machine

\paragraph{Fractal}
In the Object Management Group's (OMG)
specification~\cite{ccmdeploy:omg06}, the commissioning model is rigid
and fixed by the model. In \fractal~\cite{Blair2009} and its
evolutions GCM and GCM/ProActive~\cite{baude:hal-01001043}, the
control of a component (\eg its commissioning) is decoupled from its
functionalities into a so called \emph{membrane} which is itself
described as a component assembly written in Java. The membrane is
handled by the \fractal runtime but the sub-assembly and its
associated codes are entirely left to the user. That is why in
Table~\ref{tab:comparison}, feature not natively supported by \fractal
are shown manually implementable, using Java. Both \emph{separation of
  concerns} and \emph{inter-comp} are well handled by \fractal thanks
to the notion of port (dependencies within the component or with other
components) adapted to the membrane. Note that only \fractal
components are supported, but the commissioning of existing
modules can be encapsulated in a \fractal component.

% Deployware
\paragraph{Deployware}
\citeauthor{flissi2008ccgrid} proposed \deployware (DW), a research
effort targetting distributed software commissioning in the context of
Grid computing~\cite{flissi2008ccgrid}. Its implementation is based on
the \fractal model. A component is called a \emph{personality} and is
associated with a fixed set of commissioning actions: install,
configure, start, manage, stop, unconfigure and uninstall. Each action
describes a sequence of tasks, written in a specific high-level
language that uses pre-defined instructions (\eg execute command, copy
a file). While there is no notion of component ports, it is possible
to express dependencies between components to initiate automatic
coordination. For instance, when the operator triggers the action
"install" on a component, the same action is triggered recursively to
its dependencies. Because some features are not entirely controlled by
the user, metrics \emph{tasks}, \emph{SIMH} and \emph{inter-comp} are
considered partially supported by \deployware. Finally, \deployware is
based on \fractal and a formal effort has been carried out on
\fractal, therefore the formal aspect support of \deployware is
considered partial.

% Ansible
\paragraph{Ansible}
For devops used to shell scripts, \ansible has become a popular
configuration management tool, since it relies on a simple syntax
written in YAML and does not require agents on administrated
servers. Tasks are managed using only SSH and Python, which are
commonly installed on every Linux distribution. In comparison, similar
tools such as \chef, \puppet or \cfengine not only require some
understanding of Ruby or a custom language, but they are built on an
agent-based architecture and require prior agent commissioning on
remote hosts. \ansible improves separation of concerns by defining
\emph{roles}, which can be seen as software components. Each role
contains files that describe a sequence of tasks. To define a
composition, a specific file called an \ansible \emph{playbook} is
used to map the desired roles to the groups of nodes they will be
applied to. Those groups of nodes are defined in a separate file
called the \emph{inventory}. When \ansible is triggered, roles and
their related tasks are sequentially executed to the associated groups
of nodes. While tasks declarations are indeed managed sequentially,
each task is executed in parallel when mapped to multiple remote
hosts, thus offering \emph{SIMH} support. Typically, an operator who
wants to commission an \apache web server and a \mysql database would
download two roles from Ansible Galaxy and register them in a
playbook. Since \ansible triggers roles in a sequential manner, if the
operator is not aware that the database must be commissioned before
the web server, she could make a mistake in the order of the roles she
declared. This makes the \emph{separation of concerns} support only
partial. Finally, as one of the possible types of tasks in \ansible is
the execution of a shell command, any script could be executed as a
task, thus allowing manually support for \emph{intra-comp-tasks}
parallelism.

% Tasks are declared in an imperative way, however, Ansible relies heavily on
% declarative modules, most of which ensure task idempotency (operations are run
% once even if called multiple times).

% Aeolus
\paragraph{Aeolus}
\citeauthor{dicosmo2014ic} proposed \aeolus, a formal component-based
model for the cloud~\cite{dicosmo2014ic}. Their component model
captures the internal states of a component commissioning process
thanks to a finite state machine. Each state can be connected to use,
provide, or conflict ports to declare dependencies between the
commissioning steps of different components. Hence, ports enable
correct coordination of the global deployment process. The deployment
procedure should then be written by the user or by an external
tool~\cite{dicosmo:hal-01233489} as a sequence of actions
leading to a different state. As a result, the internal of each
component must be known. For this reason, \emph{separation of
  concerns} support is only partial. Furthermore as the deployment
procedure is a sequence and as parallel transitions cannot be defined
the \emph{intra-comp-tasks} parallelism level is only possible in a
manual way (not automated).

% Juju
\paragraph{Juju}
Canonical has developed their own software commissioning solution,
\juju\footnote{\url{https://jujucharms.com/}}, which aims at commissioning any
kind of application on various cloud providers (\eg AWS,
OpenStack) and types of resources (container, VM or
bare-metal). Its concepts are close to those of component
models. Software modules are packaged as \juju \emph{charms} that
describe the software commissioning steps through a set of scripts
called
\emph{hooks}. Operators define their composition in a specific file
called \emph{bundle} in which they declare the desired charms with
their \emph{relations}. A relation is declared between two charms and
used for component synchronization (by triggering hooks) and data
sharing at runtime, similarly to component ports. As the concepts
behind \juju resemble those of \aeolus, the metrics are similar with
the exception of the formal aspect.
% good soc, download charms and run juju deploy

\begin{table*}[tp]
  \centering
  \small
  \input{tables/tab_related_work.tex}
  \caption{Comparison of commissioning solutions based on aspects
  regarding software engineering (SE), parallelism and formalism. A supported metric is denoted
  \checkmark and counts for 3 points in the score; a partially
  supported metric is denoted (\checkmark) and counts for 2 points; a manually supported metric is denoted \emph{M} and counts for 1 point; and finally a non-supported metric is denoted -.}
  \label{tab:comparison}
\end{table*}

\paragraph{TOSCA}
\revised{The \emph{Topology and Orchestration Specification for Cloud
Applications} (\tosca) is another component-oriented model that
partially addresses the commissioning of its
components. \tosca~\cite{tosca:web,brogi2018} is a standardization
effort from OASIS to describe cloud applications, their components and
their deployment artifacts, using standard languages (\ie XML,
YAML). \tosca has multiple implementations, such as OpenTOSCA~\cite{Binz2013}
or Cloudify. A \tosca description (or template) corresponds to a graph where
nodes represent \tosca resources (\eg software components, virtual
machines, physical servers), and where edges represent the relations
between these nodes. Artifacts (of any type: scripts, executable etc.)
can be added to \tosca descriptions in a CSAR (Cloud Service ARchive)
to detail commissioning steps. Those commissioning steps can thus be
customized by the developer. Therefore, the feature associated with
the \emph{tasks} and \emph{intra-comp-tasks} metrics can be handled
manually by the user. \tosca also provides the notion of workflow, which could be assimilated to a customized lifecycle made of a sequence of operations that are usually hidden within artifacts of nodes.
This allows to define more precise life-cycles within \tosca, but require a hand-coded specification of workflows of operations by the user. To the best of our knowledge \tosca is able to handle \emph{SIMH}, \emph{inter-comp}, and partially \emph{inter-comp-tasks} when manually handling workflows. Finally, the \tosca standard~\cite{7561358} is formally defined to an extent.
}

% As there is no way to declare dependencies
% between artifacts of components, \emph{inter-task} parallelism is not
% supported, however, relations between components make both \emph{SIMH}
% and \emph{inter-comp} parallelism theoretically available
% in \tosca. No information has been found on the complete support of
% these features in \tosca
% implementations~\cite{cloudify:web,opentosca:web}. 

% Kubernetes
\paragraph{Kubernetes}
\revised{Initiated by Google, \kubernetes (K8S) is a popular framework to
commission distributed software in the form of microservices that are
packaged as a hierarchy of Docker containers managed by \emph{pods}. A
software component in \kubernetes is defined as a Docker
container. These components have no ports to manage coordination, and
their internals are fixed, since containers can only be started and
stopped. As a consequence, the commissioning process is
error-prone. For instance, a web server can be started before the
required database and thus fail. 
%For this reason, \emph{inter-comp}
%parellelism is only partially supported. 
\kubernetes requires
container to be started in any order, therefore any container must
embed a waiting procedure w.r.t.\ its dependencies. If the deployment of
a container fails, \kubernetes automatically retries. For stateless
microservice architectures, this deployment strategy is popular.
\kubernetes has been extended with workflow engines though. \argo~\footnote{https://argoproj.github.io/argo/examples/} is an open source container-native workflow engine for orchestrating parallel jobs on \kubernetes. If \argo is initially thought to execute scientific workflows it can also be used to add dependencies between containers, unlike the basic usage that launches all containers simultaneously and retries starting operations failures. However, one task in \argo models the starting of one container, and the lifecycle of a container is still limited to either started and stopped. As a result, when two containers have a dependency, the first must stop before the second can start. For this reason we consider that \kubernetes offers the \emph{inter-comp} parallelism level even if \argo is needed to fully support this feature.
}

%----------------------------
\subsection{Discussions}
%----------------------------
% 3. discussion
% - flexibility vs automation vs formal
% - separation of concerns
% - intra-task parallelism
% - performance model and formal

We have compared eight solutions according to software engineering metrics, parallelism metrics and one metric regarding the formal definition of the considered solution. Table~\ref{tab:comparison} summarizes this comparison and raises a few key points that we discuss in the following section.

As usual when working on programming languages, the existing tools
illustrate the difficult trade-off between flexibility and
automation. On the one hand, when the tool is highly programmable,
developers have the ability to manage their own code organization and
to handle any kind of software engineering or efficiency property. For
instance, by using shell scripts, any feature that we took into
account could be handled. However, each of them would have to be
hand-coded, which is difficult and error-prone. On the other hand,
some solutions such as \deployware and \juju restrict the internal
commissioning behavior of components to a fixed set of actions (\eg
install, configure, start). This guarantees full control of the
automated parallelism level, but also prevents potential optimizations
allowed by the specifics of each component.

\aeolus is the solution with the highest score according to our
metrics. Indeed, \aeolus combines advantages of component models to
structure the code of software commissioning and enhance its
separation of concerns, while introducing an additional way to model
the internal commissioning behavior of each component through
tasks. It seems that \aeolus offers a good trade-off between
flexibility and automation. However, it handles only partial
separation of concerns.

Furthermore, no existing solution offers full support for
\emph{intra-comp-tasks} parallelism. Although a few solutions already
offer a way to model the internal commissioning behavior of each
component by using tasks, dependencies between those tasks are limited
to a sequential order, thus making \emph{intra-comp-tasks} parallelism
impossible. This could be handled manually in some of the existing
tools, however these parallel aspects are difficult to implement, and
should preferably be handled automatically.

Although introducing more parallelism opens up potential performance
gains, it also introduces more complexity for the user. For this
reason, formalizing the commissioning solution is of high importance
to guarantee properties of the commissioning, such as attainability.

% One further aspect of the level of programmability provided by commissioning
% tools. When the tool is highly programmable, developers have the ability to
% manage their own code organization. This is an important aspect to finely
% express what can be executed in parallel (thus it impacts performance). For
% instance \deployware and \juju limit the internals of their modules to a
% predefined set of actions (eg config, install) while such actions might contain
% instructions that can be declared parallelable.
% %
% Our solution differs by letting developers abstract a set of instructions in the
% form of transitions. Transitions can be expressed in parallel, and their
% synchronisation is declared using states.
% %high programmable can also  improve maintainability by isolating specfic code
% %in a transition.

% Finally, a desired feature for distributed software commissioning is automation.
% Typically an operator should download the desired modules to compose a
% distributed software, instantiate and connect them through their ports, then run
% the commissioning process. This aspect requires an operational semantic to
% express how the commissioning process automatically progresses with respect to
% inter and intra-component dependencies. For instance, \aeolus, which supports
% only inter-component parallelism, is a component-based model that relies on
% states and transitions to manage inter-component dependencies. However, since
% the model lacks an operational semantic, each operation is planned and executed
% by an external scheduler. In this work, one of our contribution is to provide a
% operational semantic which drives the execution of the distributed software
% commissioning, and will be explained in \cref{subsec:operational_semantics}.

% As depicted in~\cref{tab:comparison}, we can conclude from this analysis that
% individually, none of the above attempts adequately provide a commissioning tool
% which can express both a high-degree of parallelism and feature software
% engineering aspects like separation of concerns.
% In the rest of this paper, we define \mad, our contribution which is inspired by
% \aeolus as a component-based model and relies on modules and ports to express all
% the parallelism levels described above. In the rest of the paper, we will
% focus the comparison of our solution with \ansible and \aeolus, since the former
% is widely used in production, while the second is a research effort that
% provides most of the above features.

