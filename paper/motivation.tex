Distributed software deployment occurs between the development of
components (functional part) and the execution of the distributed
software on the infrastructure (management part). As a result,
commissioning relies on information about both the behavior of the
components and the infrastructure on which they will be executed, a
frontier that is often called the DevOps domain. Note that our
definition of software commissioning does not include placement and
scheduling optimization problems, nor reconfiguration aspects of the
distributed software management.

%%\SR{It's not entirely clear that the footnotes are examples. Is there
%%  a better way to integrate them?}
%\CP{Sligthy modified the footnote to be more clear}
%\CP{Please refer to an English documentation not a French one :)} fixed

Commissioning distributed software requires coordinated interactions
with various interfaces. Firstly, components have \emph{control
  interfaces} provided by their developers, \eg \texttt{start},
\texttt{stop} or \texttt{update}. Secondly, configuration files are
used as \emph{configuration interfaces} to obtain, from system
administrators or operators, information that is not known when
writing the functional code of the component. Thirdly, components may
require packages or libraries to be installed on the host in order
to work properly. Those requirements are directly related to
\emph{infrastructure management} and may be handled through
virtualization or on physical nodes by sysadmins.
%
In the end, the commissioning procedure of a single component is a
\emph{coordination program} between these three kinds of
interfaces. Commissioning procedures are often explained in
\texttt{README} files, or in
documentation\footnote{Cf.\ an Apache example at \url{https://ubuntu.com/server/docs/web-servers-apache}}. The
deployment coordination of a single component can be simplified, \eg
by using deployment scripts or ready-to-use virtual images. In these
cases, the deployment is automated and written once. However, because
of the specificities of each infrastructure and application, these
tools need to be parametric. Finding suitable scripts and images is
often a challenge. For this reason, deployment coordination procedures
remain difficult even for a single component.
%
When considering a complete distributed software composed of a set of
components, the picture becomes even more complex, because the
commissioning procedures of various components, designed by different
developers and interacting with each other, must be coordinated. For
instance, when installing a very basic Apache/MariaDB system,
additional documentation is
required\footnote{Cf.\ a LAMP     example at \url{https://ubuntu.com/server/docs/lamp-applications}} to combine the
components. The commissioning of Spark on top of
Yarn\footnote{Cf.\ an example at \url{https://spark.apache.org/docs/latest/running-on-yarn.html}}
is another example.
%One can note that in this case, virtual images do not necessarily simplify the coordination as configuration parameters and additional configurations are needed, \eg such as virtual network configuration.
%
This complex coordination is usually the work of a \emph{devops}
engineer. With the increasing complexity of distributed software
deployment, the number of languages and tools for the devops community
has grown considerably in recent years.

%%% precision on the efficiency
% SR: "needs to be motivated more", "may seem useless": this
% vocabulary undermines the importance of the point that you
% make. Instead we could say that "efficiency has been neglected in
% previous work". And the importance of efficiency should appear in
% the intro.
Efficiency is an often neglected aspect of commissioning, but let us show
that its importance should not to be underestimated. First, the
commissioning of complex distributed software is very
time-consuming. For example OpenStack (Section~\ref{sec:use-cases})
can require in excess of one hour to deploy, because existing
solutions do not favor parallelism, and therefore exploit only a
fraction of the capabilities of the infrastructures that they
target. Second, although common sense would dictate that commissioning
occurs only once, this is not the case. System administrators perform
the commissioning process every time a new machine or new cluster is
installed in their infrastructure, when errors occur, or when updates
are needed.
%
Furthermore, with Continuous Integration (CI), Continuous Delivery
(CD), and Continuous Deployment (CDep) of companies, research or open
source projects, software commissioning is executed repeatedly in
order to test new features continuously. For instance, the traces of
the OpenStack Continuous Integration
platform\footnote{\url{http://logstash.openstack.org}} show that over
a nine-day period, from Februrary 19 to February 27, 2020, the
Kolla\footnote{\url{https://wiki.openstack.org/wiki/Kolla}} deployment
of OpenStack was executed almost 3000 times, an average of more than
300 time per day. This period did not precede a release, so even
higher numbers could be expected.
% SR: next sentence not needed
%% These numbers will be detailed in
%% Section~\ref{sec:use-cases} and are available on the reproducible lab
%% of the paper.
% SR: this seem unmotivated and out of context
%% Finally, having an efficient deployment procedure is a first step
%% toward efficient reconfiguration (\eg self-adaptive systems) and
%% dynamic infrastructures (\eg edge computing) where disruption time of
%% services must be minimized.

%\SR{Either remove the next paragraph, or explain why we focus on parallelism rather than the rest?}

Many different techniques can be studied to improve the
efficiency of distributed software deployments, notably using
optimized and adapted system commands in commissioning scripts and
programs (\eg \texttt{rsync} instead of many \texttt{scp}); working on
the optimization of a given system command (\eg \nix package manager
instead of \texttt{apt-get}); if using virtual images or container
images, improving the boot time of
hypervisors~\cite{nguyen:hal-02172288}; if using \docker images,
optimizing the placement of image layers on the
network~\cite{darrous:hal-01745405}; facilitating parallelism through
commissioning languages~\cite{dicosmo:hal-01233489}. This paper
focuses on the last option.

%\SR{The motivation section doesn't really focus on the formalization aspect}
Parallelism generally adds to the complexity of execution models,
because a parallel program can execute in many different ways based on
non-deterministic interleavings of instructions. This can create
difficulties in the development of parallel programs, including
software commissioning procedures. Since added performance should not
come at the cost of correctness, it is important to mitigate this
issue. Various strategies can be used, notably good programming
practices and software verification, but all require an unambiguous
definition of the semantics of the programming model. Developers need
to understand the various possible behaviors of the software,
independenly of the order of execution, which can be affected by
factors outside of their control, \eg implementation, communications,
execution speed, etc. Software verification techniques for their part
operate entirely on abstract models that need to accurately reflect
the ultimate implementation of the software. For these reasons, we
argue the importance of providing formal semantics for a distributed
software commissioning tool, as they provide better precision than
natural language descriptions. Indeed, conceiving the formal semantics
along with the model, as opposed to providing semantics for an
existing solution, allows for better integration.

%%% the three important metrics of this contribution
Ultimately, we pay particular attention to three factors to
assess the quality of the automation of distributed software
commissioning: (1) separation of concerns between the different
actors; (2) efficiency, \ie level of parallelism; and (3)
formalization of the solution. We give an overview of the related work
based on these aspects.
