%This paper focuses on one specific challenge related to distributed
%software deployment: \emph{distributed software commissioning}. By
%software commissioning we mean the complete installation,
%configuration and testing process when deploying distributed software
%on physical distributed resources, with or without a virtualization
%layer in between. This process is complex and error-prone because of
%the specificity of the installation process according to the operating
%system, the different kinds of virtualization layers used between the
%physical machines and the pieces of software, the amount of possible
%configuration options~\cite{Xu:2015:SAT:2775083.2791577}. Recently,
%commissioning (or configuration) management tools such as
%Ansible\footnote{\url{https://www.ansible.com/}}, or
%Puppet\footnote{\url{https://puppet.com}}, have been widely adopted by
%system operators. These tools commonly include good
%software-engineering practices such as code reuse and composition in
%management and configuration scripts. It is nowadays possible to build
%a new installation by assembling different pieces of existing
%installations\footnote{\url{https://galaxy.ansible.com/}}\footnote{\url{https://forge.puppet.com/}}
%which improves the productivity of system operators and prevents many
%errors. Many distributed software commissioning are nowadays written
%with one the three above tools and by using containers between the
%host operating system and the pieces of software, such that
%portability of installations is improved. For instance, OpenStack,
%which is the de-facto open source operating system of Cloud
%infrastructures, can be automatically installed on clusters by using
%the
%\href{https://docs.openstack.org/kolla-ansible/latest/}{\emph{kolla-ansible}
%  project}, which uses both Docker containers and Ansible.
%
%Yet, even for such well-established software, there is still much room
%for improving the efficiency of the commissioning process (\ie
%reducing deployment times, minimizing services interruptions etc.).

% SR: "IT hardware (IoT, CPUs, GPUs, Raspberry Pis, etc.)": this enumeration mixes different things (one rather vague concept, two types of processing units, one brand)
%In contrast to monolithic software running on a single machine => CP: FAUX
Distributed software is designed in a modular architectural style
where each component (a term that, in this paper, refers indifferently
to a module, a service, or a microservice) is responsible for a
specific part of the overall objective, and collaborates at runtime
with other components that are potentially hosted on distant
machines. With the advents of IT hardware, distributed software has
become commonplace, whether it be executed in the cloud, on personal
computers, mobile phones or objects.

However, deploying a large distributed software system on
distributed infrastructures poses multiple challenges. Firstly, deployment
involves a variety of actors with different roles and areas of
expertise, from \emph{developers} who are responsible for designing
and coding the components, to \emph{sysadmins} and \emph{sysops} who
are responsible for maintaining, configuring, and testing multi-user
computer systems, with \emph{devops} engineers working between them to
oversee code releases and deployments. Secondly, deployment is a
complex task: components must be chosen, configured and mapped to
infrastructure nodes, dependencies have to be solved, virtualization
layers handled, etc. The process is error-prone, and difficult to test
because faults can be caused by very specific hardware or software
conditions in a heterogeneous environment. Lastly, deployment is an
often time-consuming process, owing to the scale of the systems. This
last point is usually neglected in the literature. As a result,
complex systems such as OpenStack can require in excess of one hour to
deploy with existing solutions that do not take full advantage of the
distributed nature of the underlying hardware. This becomes especially
problematic in the context of continuous integration, with systems
being deployed up to several hundreds of times every day.

For these reasons, programming models and tools need to be developed
to facilitate documented, verifiable and efficient deployment
procedures. To this end, we propose \mad, a model that relies on a
declarative description of dependencies to automate the execution of
deployments with a high level of parallelism, both between components
and with them. Prior to execution, \mad deployments can also be
analyzed for the purpose of verification and performance estimation,
making it a useful tool during the conception of the deployment.
% SR: this description of Madeus can be extended

% SR: not really sure how to fit this paragraph in the rest of the intro
In this paper, among the difficulties related to the deployment of
distributed software systems, we leave aside the mapping between
pieces of software and the nodes of infrastructure, an optimization
problem that is not in the scope of this paper~\cite{6409358,
10.1007/978-3-319-47677-3_15, cadorel:hal-02165835, ccgridemile,
10.5555/2432523.2432528}. Indeed, we restrict the scope of this paper
to the \emph{software commissioning} part of the deployment, \ie the
procedure responsible for leading the set of components to a valid
running state while guaranteeing correct configurations and
interactions.  In particular, three metrics are considered to measure
the quality of automation of distributed software commissioning: (1)
separation of concerns between the different actors of the
commissioning procedure to enhance productivity, maintainability and
reusability of deployment code; (2) efficiency of the commissioning
procedure in terms of parallelism expressiveness; and (3)
formalization of the solution, such that formal properties can be
proven on a given commissioning procedure. The contribution of this
paper improves upon the related work by succeeding in the combination
of these three metrics.

We have previously presented \mad in~\cite{chardet:hal-01858150}. This
journal paper brings the following additional contributions compared
to our previous publication:
\begin{itemize}
\item an extended study of the related work (Section~\ref{sec:related_work});
\item a concrete language and a prototype (presented in
  Section~\ref{sec:mad_model}, along with the overview of \mad);
\item a revised and streamlined formal model that offers stronger
  guarantees (Section~\ref{sec:formal_model});
\item a theoretical performance prediction model
  (Section~\ref{sec:perf_model});
\item an extended reproducible evaluation on both synthetic use cases
  (Section~\ref{sec:evaluations}) and one large use case on real
  infrastructures (Section~\ref{sec:use-cases}).
\end{itemize}

% SR: merged with the list of contributions
%%% outline
%% The rest of this paper is organized as
%% follows. Section~\ref{sec:motivation} presents the detailed
%% motivations of the contribution. Section~\ref{sec:related_work}
%% studies the related work. Section~\ref{sec:mad_model} gives an
%% overview of \mad as well as its concrete
%% language. Sections~\ref{sec:formal_model} and~\ref{sec:perf_model}
%% respectively detail the formalization of \mad as well as its
%% associated performance prediction
%% model. Sections~\ref{sec:evaluations} and~\ref{sec:use-cases}
%% respectively present an evaluation of the \mad prototype on synthetic
%% use-cases and an evaluation of the contribution interests on the real
%% use-case of OpenStack commissioning.  Finally, Section~\ref{sec:conc}
%% concludes and gives some perspectives.




