% 1. introduce a bit the context (maybe in the introduction)
% - languages for dist. soft. commissioning
% - efficiency of dist. soft. commisioning
% - selected subset of RW and why

%--------
\subsection{Distributed software commissioning}

% correction: software is uncountable
Distributed software is software designed in a modular fashion
where each module, component, or service is responsible for a
% un or en trop (au milieu)?
specific part of the overall objective, and collaborates at runtime
with other components that are potentially hosted on distant nodes
(\ie machines) in the network. In the rest of this paper we will
use the generic term \emph{component} to refer
indifferently to a module or a service.
%
The commissioning procedure of a distributed software is the procedure
responsible for leading the set of components to a valid running state
while guaranteeing correct configurations and
interactions. Thus, distributed software commissioning is somewhere
between the components' development (functional part) and the running
distributed software on the infrastructure (management part). As a
result, the particularity of a distributed software commissioning is
that it needs information both from the components behaviors and from the
underlying infrastructure on which each component will be executed.
% who are the actors of commissionings?
For this reason, multiple human actors are often involved in the
commissioning procedure: developers and system administrators or
operators. This frontier is often called the DevOps domain where the
commissioning fits in.

First, the code of each component is written by a given
\emph{developer}, and this developer often also codes a set of
\emph{control interfaces} to act on the component. Examples of control
interfaces could be \texttt{start}, \texttt{stop} or \texttt{update},
for instance. Most of the time such control interfaces take part in
the component commissioning. However, they are not sufficient as they
only serve the component level, not the infrastructure level. Thus,
\emph{configuration files} are often used additionally as
\emph{infrastructure interfaces} to get information from system
administrators or operators that cannot be known in advance when
writing the functional code of the component. Thirdly, components often
require a set of packages or libraries to be installed on the host
operating system to work properly. Those requirements are directly
related to the \emph{infrastructure management} as some of them may
already be installed on machines. In the end, the overall
commissioning procedure of a single software component is complex because
it juggles with the control interfaces, infrastructure interfaces and
infrastructure management information. In other words, the
commissioning procedure of a single component is a \emph{coordination
  program} between different DevOps interfaces. Most of the time
commissioning procedures are explained in \texttt{README} files, or in
documentation\footnote{apache red hat}.

% correction: documentations (plural) is very uncommon, the uncountable form is preferred

When considering a complete distributed software composed of a set of
components, the picture becomes even more complex as it is needed to
coordinate the commissioning procedure of all the components together
with their associated interactions. For instance, when installing a
very basic Apache/MariaDB system, an additional documentation is
required\footnote{red hat} to combine the Apache component with the
MariaDB component. Another example could be the commissioning of Spark
on top of Yarn\footnote{spark on Yarn}.

\HC[Dim]{ce listing ne va sans doute pas ici (trop tôt ou top détaillé) mais je le laisse}
Many interesting challenges arise from the DevOps community and from
distributed software commissioning. The challenges studied in this
paper are the following:
\begin{itemize}
\item enhancing the automation of distributed software commissionings;
\item enhancing the separation of concerns between developers and
  operators in distributed software commissionings;
\item enhancing and adapting composition mechanisms in software
  commissionings;
\item enhancing the efficiency of distributed software commissionings;
\item promoting formal models and formal methods to have guarantees on
  distributed software commissionings.
\end{itemize}

%--------
\subsection{Automation of distributed software commissioning}
% 1. scripting
% 2. infrastructure as code
% 3. provisioning tools
% 4. orchestration tools
% 5. scope of the paper
% - software commissioning of a single distributed software
% - generic solution that could use or not provisioning tools
% - scripting/IaC but keep Kubernetes and Juju

Four classes of tools and languages can be considered regarding
automating and enhancing software engineering properties
of commissioning procedures.
\CS[HC]{on améliore les propriétés d'ingénierie logicielle ? Je pensais
  que c'était les garantir, et du coup enforce pas enhance}


\paragraph{Scripting/coding languages}
First, a common way of automating commissioning procedures is to use
\emph{scripting languages} such as \shell or \ruby for instance. Such
languages are very flexible and well known by system administrators
and operators but suffer from many limitations, particularly from a
software engineering standpoint. In particular, they are not well
suited to deal with separation of concerns, composition and complex
coordination mechanisms.

\paragraph{Software configuration tools}
A second class of languages and tools is called \emph{software
  configuration tools}. This class contains the set of quite recent
DevOps tools such as \ansible~\cite{ansible:web},
\puppet~\cite{puppet:web}, \chef~\cite{chef:web},
\salt~\cite{salt:web}, as well as some academic contributions
\deployware~\cite{flissi2008ccgrid},
\aeolus~\cite{dicosmo2014ic,dicosmo:hal-01233489} etc. The goal of
these tools is to enhance the productivity when defining components or
services commissionings and the coordination of their tasks. Most of
the time these tools add an abstraction layer on top of scripting languages
to hide some of their technical details (\eg SSH connections). These
tools use different methods to achieve their goals. For instance,
Ansible adopts a procedural imperative style in YAML as a
series of tasks to execute, while Puppet adopts a declarative approach
in Python by combining different instances of resources (\ie
services, packages, etc.). In a declarative approach the user describes
what is needed rather than how to get it, which is left to the tool
developers. Such an approach enhances productivity but reduces
flexibility for the user.

\paragraph{Infrastructure definition tools}
Some tools, that are sometimes called \emph{infrastructure definition
  tools}~\cite{}, or \emph{provisioning tools}, have been specifically
designed to be able to handle complex distributed infrastructures
composed of multiple clusters and multiple machines shared between
users. Managing such infrastructure is very difficult and error prone
as each application and each user may possibly need different
requirements, different operating systems, different package versions,
etc. Virtualization has partly been introduced to enhance
infrastructure management (in addition to improving portability of
applications, isolation between users, etc.). This type of tools, that
includes \docker~\cite{docker:web}, \terraform~\cite{terraform:web},
\juju~\cite{juju:web}, \cloudformation~\cite{cloudf:web} or
\heat~\cite{heat:web} for instance, uses virtualization mechanisms (\eg
virtual machines, containers) to reduce the commissioning process to a
set of commands allowing to deploy a virtual resource (by using an image) on a
physical machine. This technique potentially reduces the complexity of
the commissioning procedures if the needed image is already available,
otherwise the initial commissioning commands and coordination have to
be done to create the image. Some of these tools are specific to a
given virtualization technique (\eg \docker, \cloudformation and
\heat), while others offer \emph{providers} for different kinds of
Cloud infrastructures (\eg \terraform, \juju). The \tosca
standard~\cite{tosca:web}, and its
implementations~\cite{Binz2013,cloudify:web,opentosca:web,8599581} can
be classified as a descriptive provisioning tool. More precisely,
\tosca is presented as a Cloud Modeling Language (CML). By using
\tosca, a set of cloud-based services is modeled as a graph where each
node represents a component of the application and each edge
represents the relationship between components. \tosca allows for
portability and automated management across cloud platforms and
infrastructures. In \tosca, commissioning actions are scripts
encapsulated in \emph{artifacts}.

\paragraph{Orchestration tools}
Finally, some recent tools go further by offering an
\emph{orchestration} level to handle shared clusters of machines
running many services simultaneously to orchestrate. In this case,
being able to deploy or install distributed software is only part of
the problem, as services also need to be restarted in case of failure, or
to be scaled in case of overload, etc. The famous
\kubernetes~\cite{kubernetes:web} enters in this category, as well as
\dockerswarm~\cite{dockerswarm:web}. If these tools are outside the
scope of this paper, a subdivision of their architecture handles
distributed software commissioning. For instance \kubernetes relies
on \docker for commissioning.

In pratice, these tools are often combined by system administrators
and operators to achieve both distributed software commissioning and
infrastructure management.

\begin{tcolorbox}[enhanced,attach boxed title to top left={yshift=-3mm,yshifttext=-1mm},
  colback=black!5!white,colframe=black!30,colbacktitle=black!60,
  title=Scope,fonttitle=\bfseries,
  boxed title style={size=small,colframe=black!60,boxrule=0.2mm},
  boxrule=0.2mm]
  The contribution of this paper, \ie the Madeus model, specifically
  focuses on enhancing the procedural-oriented \emph{software
    configuration tools} by improving separation of concerns,
  composition and efficiency.
\end{tcolorbox}

%In
%Section~\ref{sec:related_work}, though, compares in details tools
%from the four classes: Bash, Ansible, Puppet, Deployware, Aeolus, Juju
%nd Kubernetes.

%--------
\subsection{Efficiency of distributed software commissioning}
% 1. Optimize the commissioning by choosing adequate commands and
% tools
% 2. if using VM or Docker images: improve the boot time (Yolo)
% 3. if using Docker images: optimize placement of chunks to reduce
% the download time (Nitro)
% 4. increase the level of parallelism of commissionings
% 5. scope of the paper = parallelism

Improving the efficiency of distributed software commissioning may
seem useless as this procedure is executed once and for all. However, this claim
does not consider the problem as a whole.
%
First, the commissioning procedure of complex distributed software
such as OpenStack (Section~\ref{sec:use-cases}) can take from 15
minutes to more than one hour to finish, thus gaining in speed is
significant. Second, a system administrator does not perform the
commissioning process only once. When new machines or new clusters are
installed in their infrastructure, or when updates are needed,
commissioning procedures need to be executed again. Third, when
designing software commissioning, errors may often occur, leading to the
re-execution of the procedure multiple times.
% reconfiguration
In addition to this, having an efficient commissioning procedure is a
first step toward efficient reconfigurations of dynamic software
systems (\eg self-adaptive systems) and dynamic infrastructures (\eg
edge computing) where disruption time of services must be minimized.
% experiments and continuous integration
Furthermore, as explained in the introduction, there are two
specific contexts where distributed software commissionings are
extensively executed: research or industry \emph{experiments} and
\emph{continuous integration} of distributed software systems. For
instance, by exploring the traces of the OpenStack CI platform, we have
observed that the OpenStack commissioning is called around 6,000 times
in 24 hours.

Many different techniques could be studied to improve the efficiency
of distributed software commissionings. A few of them are given below:
\begin{itemize}
\item using optimized and adapted system commands in commissioning
  scripts and programs (\eg \texttt{rsync} instead of many
  \texttt{scp});
\item working on the optimization of a given system command (\eg
  \nix package manager instead of \texttt{apt-get});
\item if using virtual images or container images, improving the
  boot time of hypervisors~\cite{nguyen:hal-02172288};
\item if using \docker images, optimizing the placement of image
  layers on the network~\cite{darrous:hal-01745405};
\item exposing more parallelism in commissioning
  languages~\cite{dicosmo:hal-01233489}.
\end{itemize}

\begin{tcolorbox}[enhanced,attach boxed title to top left={yshift=-3mm,yshifttext=-1mm},
  colback=black!5!white,colframe=black!30,colbacktitle=black!60,
  title=Scope,fonttitle=\bfseries,
  boxed title style={size=small,colframe=black!60,boxrule=0.2mm},
  boxrule=0.2mm]
  The scope of this paper is to study how to improve the last point of
  the items above, \ie offer more parallelism in procedural
  commissioning tools and languages.
\end{tcolorbox}
